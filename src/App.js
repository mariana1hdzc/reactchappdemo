import React, { Component } from 'react';
    import './App.css';
    import Post from './Post/component/Post';
    import PostEditor from './PostEditor/component/PostEditor';
    import firebase from 'firebase/app';
    import 'firebase/database';

class App extends Component {
    constructor(props) {
    super(props);
      this.state = {
        posts : []
      }
      // hack para asegurar que las funciones tiene el contexto del componente
    this.addPost = this.addPost.bind(this);
    this.updateLoacState = this.updateLoacState.bind(this);
    this.componentWillMount = this.componentWillMount.bind(this);
    // config copiado de firebase ya que hemos creado ahi el proyecto
    const config = {
    apiKey: "AIzaSyDKyT4b8hPU17ApCxCyHw7Q29Ho2cv_O_8",
    authDomain: "reactchatappdemo.firebaseapp.com",
    databaseURL: "https://reactchatappdemo.firebaseio.com",
    projectId: "reactchatappdemo",
    storageBucket: "reactchatappdemo.appspot.com",
    messagingSenderId: "22833372156"
  };
// referencia al objeto de firebase
    this.app = firebase.initializeApp(config);
//referencia a la base de datos
    this.database = this.app.database();
// referencia a la "tabla" post
    this.databaseRef =this.database.ref().child('post');
    }
    addPost (newPostBody, newPostUser) {
    // copiar el estado
    const postToSave = {name: newPostUser, message: newPostBody};
   // guardar en posts
   this.databaseRef.push().set(postToSave);
  }
  componentWillMount() {
    const {updateLoacState} = this;
    this.databaseRef.on('child_added', snapshot => {
       const response = snapshot.val();
       updateLoacState(response);
    });
  }
  updateLoacState(response) {
   // copia del estado actual
    const posts = this.state.posts;
    // separar por saltos de linea (no se ven en html)
     //const brokenDownPost = response.message.split(/[\r\n]/g);
    // actualizar la copia del estado
  posts.push(response);
    // actualizar el estado
    this.setState(posts);
  }   

      render() {
        console.log(this.state);
        return (
          <div>
<div className="page-header centered">
  <h1>React Chat App <small>with firebase</small></h1>
</div>
          <div className="panel panel-primary post-editor"> 
          
            <div className="panel-heading"> 
              <h3 className="panel-title">Mensajes en el chat</h3> 
            </div> 
            <div className="panel-body">
            {
                this.state.posts.map(({message, name}, idx) => {
                  return (<Post key={idx} postBody={message}  userName={name}/>)
                })
              }
          </div>
          </div> 
            <div className="panel panel-primary post-editor"> 
              <div className="panel-heading"> 
                <h3 className="panel-title">Escribe tu mensaje</h3> 
              </div>

              <PostEditor addPost={this.addPost} />
              </div>
          </div>
        );
      }
    }

    export default App; 